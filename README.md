# KNOWLEDGE DISTILLATION IN NEURAL NETWORKS

In our study, we focus to minimize the need for high memory usage and high 
processing capacity, which is an important disadvantage of the area of deep neural 
networks, which is used in computer vision, object detection, and data processing and has 
undergone significant developments in recent years. The fact that deep neural networks 
create such high system resource requirements on the device on which they work, 
prevents them from being used on mobile or wearable devices with low system resources.

